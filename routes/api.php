<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['middleware' => 'auth:api'], function () {
	
	Route::get('/client', 'ClientController@index');
	Route::get('/client/{client}', 'ClientController@show');
	Route::delete('/client/destroy/{client}', 'ClientController@destroy');
	Route::post('/client', 'ClientController@save');
	Route::put('/client', 'ClientController@save');


	Route::get('/product', 'ProductController@index');
	Route::get('/product/{product}', 'ProductController@show');
	Route::delete('/product/destroy/{product}', 'ProductController@destroy');
	Route::post('/product', 'ProductController@save');
	Route::put('/product', 'ProductController@save');


	Route::get('/purchase', 'PurchaseController@index');
	Route::get('/purchase/{purchase}', 'PurchaseController@show');
	Route::delete('/purchase/destroy/{purchase}', 'PurchaseController@destroy');
	Route::post('/purchase', 'PurchaseController@save');
	Route::put('/purchase', 'PurchaseController@save');


});
