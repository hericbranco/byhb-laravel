<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'HomeController@index')->name('home');
Route::get('/', function(){
	return redirect(Lang::getLocale().'/purchase');
})->name('home');

Route::get('/alter_lang/{locale}', function($locale){
	App::setLocale($locale);
	return redirect(Lang::getLocale().'/purchase');
});

//Route::get('/about', 'PagesController@about');
Route::get('/trello', 'TrelloController@index');

Route::get('/session/trello', 'SessionController@trello')->name('trello_signin');


//Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store')->name('register');

Route::post('/logout', 'SessionController@destroy')->name('logout');
Route::get('/password_request', function(){})->name('password.request');
Route::get('/login', 'SessionController@create')->name('login');
Route::post('/login', 'SessionController@store');



Route::get('/updateTrelloToken', function(){
	return view('registration.updateTrelloToken');
});

Route::get('/storeTrelloToken/{user}', 'RegistrationController@storeToken');


Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/profile/create', 'ProfileController@create');
Route::get('/profile/edit/{profile}', 'ProfileController@edit');
Route::delete('/profile/destroy/{profile}', 'ProfileController@destroy');
Route::post('/profile', 'ProfileController@save');


Route::get('/client', 'ClientController@index')->name('client');
Route::get('/client/create', 'ClientController@create');
Route::get('/client/edit/{client}', 'ClientController@edit');
Route::delete('/client/destroy/{client}', 'ClientController@destroy');
Route::post('/client', 'ClientController@save');


Route::get('/product', 'ProductController@index')->name('product');
Route::get('/product/create', 'ProductController@create');
Route::get('/product/edit/{product}', 'ProductController@edit');
Route::delete('/product/destroy/{product}', 'ProductController@destroy');
Route::post('/product', 'ProductController@save');


Route::get('/purchase', 'PurchaseController@index')->name('purchase');
Route::get('/purchase/create', 'PurchaseController@create');
Route::get('/purchase/edit/{purchase}', 'PurchaseController@edit');
Route::delete('/purchase/destroy/{purchase}', 'PurchaseController@destroy');
Route::post('/purchase', 'PurchaseController@save');


Route::get('/users', 'UsersController@index')->name('users');
Route::get('/users/create', 'UsersController@create');
Route::get('/users/edit/{user}', 'UsersController@edit');
Route::delete('/users/destroy/{user}', 'UsersController@destroy');
Route::post('/users', 'UsersController@save');

Route::get('/telegram', 'TelegramController@getHome');
Route::get('/telegram/get-updates',   'TelegramController@getUpdates');
Route::get('/telegram/send',  'TelegramController@getSendMessage');
Route::post('/telegram/send', 'TelegramController@postSendMessage');