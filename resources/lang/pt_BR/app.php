<?php 
return [
	'created' => 'Criado',
	'actions' => 'Ações',
	'add' => 'Adicionar',
	'list' => 'Listagem',
	'back' => 'Voltar',
	'save' => 'Salvar',
	'register' => 'Cadastro',

	'clients' => 'Clientes',
	'products' => 'Produtos',
	'purchases' => 'Pedidos de Compra'
];