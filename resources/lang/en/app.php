<?php 
return [
	'created' => 'Created',
	'actions' => 'Actions',
	'add' => 'Add',
	'list' => 'List',
	'back' => 'Back',
	'save' => 'Save',
	'register' => 'Form',

	'clients' => 'Clients',
	'products' => 'Products',
	'purchases' => 'Purchases'
];