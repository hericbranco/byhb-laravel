<div class="row">
	<div class="col-md-12">
		<div class="form-group">		
			{!! Form::label('name', __('client.name'), ['class' => 'form-label']) !!}			
			{!! Form::text('name', null, ['class' => 'form-control']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('email', __('client.email'), ['class' => 'form-label']) !!}			
			{!! Form::text('email', null, ['class' => 'form-control email']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('cpf', __('client.cpf'), ['class' => 'form-label']) !!}			
			{!! Form::text('cpf', null, ['class' => 'form-control cpf']) !!}			
		</div>
	</div>
</div>