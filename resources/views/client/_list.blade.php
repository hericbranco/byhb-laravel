<table id="searchTable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>{{__('client.name')}}</td>
            <td>{{__('client.email')}}</td>
            <td>{{__('client.cpf')}}</td>
            <td>{{__('app.created')}}</td>
            <th>{{__('app.actions')}}</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($arrObjData AS $objData)
            <tr>
                <td>{{$objData->name}}</td>
                <td>{{$objData->email}}</td>
                <td>{{$objData->cpf}}</td>
                <td>{{$objData->created_at}}</td>
                <td>@include('layouts._list_actions', ['objData' =>$objData])</td>
            </tr>
        @empty

        @endforelse
    </tbody>

</table>