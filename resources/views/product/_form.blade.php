<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			{!! Form::label('name', __('product.name'), ['class' => 'form-label']) !!}			
			{!! Form::text('name', null, ['class' => 'form-control']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('barCode', __('product.code'), ['class' => 'form-label']) !!}			
			{!! Form::text('barCode', null, ['class' => 'form-control']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('price', __('product.price'), ['class' => 'form-label']) !!}			
			{!! Form::text('price', null, ['class' => 'form-control']) !!}			
		</div>
	</div>
</div>