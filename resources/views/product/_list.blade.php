<table id="searchTable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>{{__('product.name')}}</td>
            <td>{{__('product.price')}}</td>
            <td>{{__('product.code')}}</td>
            <td>{{__('app.created')}}</td>
            <th>{{__('app.actions')}}</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($arrObjData AS $objData)
            <tr>
                <td>{{$objData->name}}</td>
                <td>{{$objData->price}}</td>
                <td>{{$objData->barCode}}</td>
                <td>{{$objData->created_at}}</td>
                <td>@include('layouts._list_actions', ['objData' =>$objData])</td>
            </tr>
        @empty

        @endforelse
    </tbody>

</table>