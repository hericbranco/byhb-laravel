{!! Form::open(['url' => App::getLocale().'/'.explode('/', Route::current()->uri)[1].'/destroy/'.$objData->id, 'method' => 'post']) !!}
{{ method_field('DELETE') }}
<a href="{{ url(App::getLocale().'/'.explode('/', Route::current()->uri)[1].'/edit', ['id' => $objData->id]) }}" title="Editar" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></a>
<button type="submit" class="btn btn-default"><i class="fa fa-trash-o"></i></button>
{!! Form::close() !!}