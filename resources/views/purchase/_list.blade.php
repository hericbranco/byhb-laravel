<table id="searchTable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>{{__('purchase.client')}}</td>
            <td>{{__('purchase.product')}}</td>
            <td>{{__('purchase.quantity')}}</td>
            <td>{{__('purchase.status')}}</td>
            <td>{{__('app.created')}}</td>
            <th>{{__('app.actions')}}</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($arrObjData AS $objData)
            <tr>
                <td><a href="{{url(App::getLocale().'/client/edit/'.$objData->client_id)}}">{{$objData->client->name}}</a></td>
                <td><a href="{{url(App::getLocale().'/product/edit/'.$objData->product_id)}}">{{$objData->product->name}}</a></td>
                <td>{{$objData->quantity}}</td>
                <td>{{$objData->status}}</td>
                <td>{{$objData->created_at}}</td>
                <td>@include('layouts._list_actions', ['objData' =>$objData])</td>
            </tr>
        @empty

        @endforelse
    </tbody>

</table>