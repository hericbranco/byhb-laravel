<div class="row">
	<div class="col-md-12">

		<div class="form-group">
			{!! Form::label('client_id', __('purchase.client'), ['class' => 'form-label']) !!}			
			{!! Form::select('client_id', $arrClient, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('product_id', __('purchase.product'), ['class' => 'form-label']) !!}			
			{!! Form::select('product_id', $arrProduct, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('quantity', __('purchase.quantity'), ['class' => 'form-label']) !!}			
			{!! Form::text('quantity', null, ['class' => 'form-control']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('status', __('purchase.status'), ['class' => 'form-label']) !!}			
			{!! Form::select('status', ['open' => 'Open', 'paying' => 'Paying', 'canceled' => 'Canceled'], null, ['class' => 'form-control']) !!}			
		</div>
	</div>
</div>