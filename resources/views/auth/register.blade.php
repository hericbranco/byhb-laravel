@extends('layouts.not-loged')

@section('content')

<div class="register-wrapper row">
    <div id="register" class="login loginpage col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 col-xs-12 col-sm-6 col-lg-4">
        <h1><a href="{{route('home')}}" title="Login Page" tabindex="-1">{{ config('app.name', 'Laravel') }}</a></h1>

        
        @include('layouts.errors')
        

        <form name="loginform" id="loginform" action="{{ route('register') }}" method="post">
            {{ csrf_field() }}
            <p>
                <label for="user_login">Nome<br>
                    <input name="name" id="user_login" class="input" value="" size="20" type="text"></label>
                </p>
                <p>
                    <label for="user_login2">Email<br>
                        <input name="email" id="user_login2" class="input" value="" size="20" type="text"></label>
                    </p>
                    <p>
                        <label for="user_pass">Senha<br>
                            <input name="password" id="user_pass" class="input" value="" size="20" type="password"></label>
                        </p>
                        <p>
                            <label for="user_pass2">Confirmar Senha<br>
                                <input name="password_confirmation" id="user_pass2" class="input" value="" size="20" type="password"></label>
                            </p>
                            <p class="forgetmenot">
                                <label class="icheck-label form-label" for="rememberme"><div class="icheckbox_minimal-aero checked" style="position: relative;"><input name="rememberme" id="rememberme" value="forever" class="icheck-minimal-aero" checked="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Eu aceito os Termos e condições</label>
                            </p>



                            <p class="submit">
                                <input name="wp-submit" id="wp-submit" class="btn btn-accent btn-block" value="Sign Up" type="submit">
                            </p>
                        </form>

                        <p id="nav">
                            <a class="pull-left" href="#" title="Password Lost and Found">Forgot password?</a>
                            <a class="pull-right" href="ui-login.html" title="Sign Up">Sign In</a>
                        </p>
                        <div class="clearfix"></div>
                        @if (!empty(config('app.arrLoginBy', array())))
                        <div class="text-center register-social">

                            @foreach (config('app.arrLoginBy', array()) AS $name => $url)
                            <a href="{{$url}}" class="btn btn-primary btn-lg {{$name}}"><i class="fa fa-{{$name}} icon-sm"></i></a>
                            @endforeach

                        </div>
                        @endif

                    </div>
                </div>
                @endsection
