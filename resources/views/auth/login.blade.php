@extends('layouts.not-loged')

@section('content')
<div id="login" class="login loginpage col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 col-xs-12 col-sm-6 col-lg-4">
    <h1><a href="#" title="Login Page" tabindex="-1">{{ config('app.name', 'Laravel') }}</a></h1>

    <form class="form-horizontal" role="form" method="POST" action="{{ url(App::getLocale().'/login') }}">
        {{ csrf_field() }}
        <p>
            <label for="email">Username<br />

                <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required autofocus></label>
            </p>
            <p>
                <label for="password">Password<br />                                
                    <input id="password" type="password" class="input" name="password" required></label>
                </p>
                <p class="forgetmenot">
                    <label class="icheck-label form-label" for="rememberme"><input type="checkbox" id="rememberme" value="forever" class="icheck-minimal-aero" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me</label>
                </p>

                <p class="submit">
                    <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-accent btn-block" value="Sign In" />
                </p>
            </form>

            <p id="nav">
                <a class="pull-left" href="{{ route('password.request') }}" title="Password Lost and Found">Forgot password?</a>
                <a class="pull-right" href="{{ route('register') }}" title="Sign Up">Sign Up</a>
            </p>
        </div>
    </div>
</div>
@endsection