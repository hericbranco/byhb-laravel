@extends('layouts.app')

@section('content')
<section class="box ">
    <header class="panel_header">
        <h2 class="title pull-left">Section Box</h2>
        <div class="actions panel_actions pull-right">
            <a class="box_toggle fa fa-chevron-down"></a>
            <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
            <a class="box_close fa fa-times"></a>
        </div>
    </header>
    <div class="content-body">    
        <div class="row">
            <div class="col-xs-12">
                <table id="searchTable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td>Projeto</td>
                            <td>Tarefa</td>
                            <td>Categoria</td>
                            <td>Responsável</td>
                            <td>Ultima Alteração</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/E9EvvALD/akracing" target="blank">Akracing</a></td>
                            <td class="taskName"><a href="https://trello.com/c/e8ltiaGp/35-1-40-refazer-o-rodape-para-que-fique-igual-ao-do-template-criado-pelo-jean" target="blank">(1) 40-Refazer o rodapé para que fique igual ao do template criado pelo Jean</a></td>
                            <td class="catName">Correção de Bug's, <br>Site</td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">31/01/2017 15:35:47</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/E9EvvALD/akracing" target="blank">Akracing</a></td>
                            <td class="taskName"><a href="https://trello.com/c/y2cXSMsZ/3-aumentar-a-logo-da-home" target="blank">Aumentar a logo da Home</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">31/01/2017 15:39:00</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/E9EvvALD/akracing" target="blank">Akracing</a></td>
                            <td class="taskName"><a href="https://trello.com/c/yhtDPn5D/39-preparando-os-diretorios" target="blank">Preparando os diretórios!!!</a></td>
                            <td class="catName">Importação/Exportação/Atualização</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">02/02/2017 14:46:23</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/E9EvvALD/akracing" target="blank">Akracing</a></td>
                            <td class="taskName"><a href="https://trello.com/c/jAlJcKuO/6-mudar-a-cor-do-texto-digitado-no-botao-de-procurar-pois-a-cor-atual-e-a-mesma-cor-de-fundo-do-balao" target="blank">Mudar a cor do texto digitado no botão de procurar. Pois a cor atual é a mesma cor de fundo do balão.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">31/01/2017 15:39:42</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/E9EvvALD/akracing" target="blank">Akracing</a></td>
                            <td class="taskName"><a href="https://trello.com/c/4QXfqqqq/32-2-22-23-refazer-o-menu-header-e-acrescentar-os-itens-seja-um-revendedor-e-compra-coorporativa" target="blank">(2) 22~23-Refazer o menu/header e acrescentar os itens "Seja um revendedor" e "Compra coorporativa"</a></td>
                            <td class="catName">Melhoria, <br>Admin, <br>Site</td>
                            <td class="memberName"></td>
                            <td class="lastActivity">01/02/2017 14:16:15</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/E9EvvALD/akracing" target="blank">Akracing</a></td>
                            <td class="taskName"><a href="https://trello.com/c/JmoTBapU/15-acrescentar-o-menu-compra-coorporativa" target="blank">Acrescentar o menu Compra Coorporativa</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">31/01/2017 20:28:35</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/E9EvvALD/akracing" target="blank">Akracing</a></td>
                            <td class="taskName"><a href="https://trello.com/c/CBhB1oyE/14-acrescentar-o-menu-seja-um-revendedor" target="blank">Acrescentar o menu Seja um revendedor</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">31/01/2017 20:28:31</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/E9EvvALD/akracing" target="blank">Akracing</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Z71L0wOU/1-conteudo-da-pagina-de-faq" target="blank">Conteúdo da página de FAQ</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">01/02/2017 14:23:02</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/uVD0dyvj/7-remover-twitter-e-g-do-bloco-social" target="blank">Remover Twitter e G+ do bloco social</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:44:45</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/GSI9wMUi/5-traduzir-links-permanentes" target="blank">Traduzir links permanentes</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 17:22:05</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/sVQfM1Cg/17-adicionar-links-de-login-e-registro" target="blank">adicionar links de login e registro</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 17:22:32</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/9sgdwj15/4-organizar-paginas-e-artigos-como-postagens" target="blank">Organizar páginas e artigos como postagens</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:44:56</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/kUSLYtoS/15-definir-as-seguintes-paginas-e-corrigir-a-chamada-no-menu" target="blank">Definir as seguintes páginas e corrigir a chamada no menu</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 17:22:28</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/NJPdr9a5/3-estruturar-categorias-para-o-blog" target="blank">Estruturar categorias para o blog</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:45:13</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/BXLmZjNq/11-verificar-menu-fixo-no-modo-responsivo" target="blank">Verificar menu fixo no modo responsivo</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:50:46</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Jr12Ah0M/2-revisar-o-top-header-no-modo-responsivo" target="blank">Revisar o top-header no modo responsivo</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 17:20:43</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Xjs3tU54/12-definir-estrutura-de-links-permanentes-para-category-sub-categories" target="blank">Definir estrutura de links permanentes para Category-&gt; Sub Categories</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:51:10</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/DeF95jHj/10-ajustar-largura-maxima-do-formulario-na-pagina-depoimentos" target="blank">Ajustar largura máxima do formulário na página depoimentos</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:51:15</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/4Es7g9FG/9-configurar-o-contact-form-7-no-tema-do-blog" target="blank">Configurar o Contact form 7 no tema do blog</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:52:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/ucFmGaeg/18-criar-formulario-de-cadastro" target="blank">criar formulário de cadastro</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 19:08:50</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/F9qK10Sx/8-analisar-a-construcao-da-area-de-enquete-na-pagina-inicial" target="blank">Analisar a construção da área de enquete na página inicial. </a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 14:50:12</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Mp6kNKE8/blog-lingua-portuguesa" target="blank">Blog Língua Portuguesa</a></td>
                            <td class="taskName"><a href="https://trello.com/c/HvuYXj5H/14-configura-categorias-do-blog" target="blank">Configura categorias do blog</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:52:52</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/awYKB2XE/22-ajustar-botao-do-carrinho-na-pagina-da-lista-de-produto-do-carrinho-botao-em-branco-sem-o-texto" target="blank">Ajustar botão do carrinho na  página da lista de produto do carrinho.Botão em branco sem o texto.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">15/12/2016 13:53:27</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/AsFjDcHh/23-mudanca-nas-cores-das-informacoes-de-alertas" target="blank">Mudança nas cores das informações de alertas.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">15/12/2016 13:53:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/KMrNQeic/20-corrigir-a-cor-do-texto-na-pagina-de-compra-carrinho" target="blank">Corrigir a cor do texto na página de compra(Carrinho).</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">15/12/2016 13:53:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/bAhVLOXf/21-acertar-a-cor-do-texto-da-pagina-finalizar-compra" target="blank">Acertar a cor do texto da página Finalizar compra.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">15/12/2016 13:53:13</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/8O9i9vPu/4-corrigir-problema-de-botao-branco-na-listagem-de-produto" target="blank">Corrigir problema de "botão branco" na listagem de produto</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">09/12/2016 12:28:44</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/gFN67z9i/9-baixar-todo-o-site-para-colocar-no-repositorio" target="blank">Baixar todo o site para colocar no repositório.</a></td>
                            <td class="catName">Importação/Exportação/Atualização</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">09/12/2016 12:28:11</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/yOz7f8VT/11-quebra-no-botao-responsivo" target="blank">Quebra no botão responsivo!!!</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:29:05</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/LmHzJwE1/10-acerto-no-botao-comprar-da-pagina-dos-produtos-e-colocar-setas-da-area-de-quantidade-colocar-nome-da-area-de-quantidade" target="blank">Acerto no botão comprar da página dos produtos e colocar setas da área de quantidade. Colocar nome da área de quantidade.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:29:26</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/hGNqKyoP/3-corrigir-problema-de-quebra-de-layout-na-pagina-de-produtos" target="blank">Corrigir Problema de quebra de layout na página de produtos</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:29:36</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/tF5xeb6i/17-ajuste-nas-cores-da-pagina-entrar-e-registrar-div-entrar-e-div-registar-alterar-a-cor-de-fundo-para-visualizar-o-texto" target="blank">Ajuste nas cores da página Entrar e registrar!!div entrar e div registar, alterar a cor de fundo para visualizar o texto.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:29:59</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/gugqfmxJ/14-alterar-botoes-e-setas" target="blank">Alterar botões e setas.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:30:29</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/5qON43p0/6-retirar-a-linha-que-diferencia-o-topo-do-resto-do-site" target="blank">Retirar a linha que diferencia o topo do resto do site</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:30:41</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/66CReop8/7-retirar-as-barras-que-dividem-o-menu-do-site" target="blank">Retirar as barras que dividem o menu do site</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:33:31</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/GjDOWQzM/16-acertar-o-responsivo-do-menu-principal" target="blank">Acertar o responsivo do Menu principal</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:33:52</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/NwKhYPyJ/5-sorrecao-do-botao-de-lupa-nao-aparecendo-no-topo-do-site" target="blank">Sorreção do botão de lupa não aparecendo no topo do site</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:34:28</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/I11FuoNh/8-alinhar-os-menus-aos-botoes-que-os-abrem-no-menu" target="blank">Alinhar os menus aos botões que os abrem no 'menu'</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">09/12/2016 12:34:43</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/l4wfeAhJ/24-corrigir-exibicao-dos-botoes-comprar" target="blank">Corrigir exibição dos botões "Comprar"</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">12/12/2016 14:08:34</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/krdIxTTq/15-espaco-no-responsivo-do-footer-reativar-o-frame-do-facebook" target="blank">Espaço no responsivo do footer, reativar o frame do facebook.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName"></td>
                            <td class="lastActivity">12/12/2016 17:13:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/f3wo1UZT/32-priorizar-produtos-em-estoque-na-exibicao-do-woocomerce" target="blank">Priorizar produtos em estoque na exibição do WooComerce</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">12/12/2016 20:27:40</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/k4fuoaDi/13-acertar-deslocamento-nos-botoes" target="blank">Acertar deslocamento nos botões.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">12/12/2016 20:28:49</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/u3fP3WcR/31-agrupar-o-menu-de-cadeiras-e-acrescentar-o-menu-hardware" target="blank">Agrupar o menu de cadeiras e acrescentar o menu Hardware.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">14/12/2016 20:46:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3TftkTvb/28-regular-a-proporcao-das-imagens-em-formas-de-pagamento" target="blank">Regular a proporção das imagens em FORMAS DE PAGAMENTO</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria</td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">14/12/2016 20:47:17</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/K0lA46LW/dt3-scrum" target="blank">Dt3 - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Qt5CVi1F/27-corrigir-o-modo-responsivo-do-footer" target="blank">Corrigir o modo responsivo do Footer</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">15/12/2016 13:34:16</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/h6NY9j9u/38-1-normatizar-campo-corretor-id-da-tabela-laudos-1" target="blank">(1) Normatizar campo corretor_id da tabela laudos [1]</a></td>
                            <td class="catName">Melhoria, <br>Importação/Exportação/Atualização, <br>Desafio</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">16/12/2016 20:16:32</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/L61kRGMz/37-corrigir-o-erro-que-ocorre-ao-selecionar-mais-de-um-bairro-na-busca-do-admin" target="blank">Corrigir o Erro que ocorre ao selecionar mais de um bairro na busca do admin</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">15/12/2016 20:16:27</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/tUG5OnHj/53-ajuste-na-busca-de-desejo-do-cliente" target="blank">Ajuste na busca de desejo do cliente</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">08/02/2017 11:09:57</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/C0e94x1B/54-ajuste-formulario-de-busca-de-laudo" target="blank">Ajuste formulario de busca de laudo</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">08/02/2017 11:09:59</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/6woWdvdP/31-adicionar-o-campo-finalidade-na-busca-do-site" target="blank">Adicionar o campo finalidade na Busca do site</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco, <br>Rafael Cruz</td>
                            <td class="lastActivity">15/12/2016 20:03:22</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/cenBfK8d/55-ajuste-busca-do-site-para-mascara-numerica-com-separador-de-milhar" target="blank">Ajuste busca do site para mascara numerica com separador de milhar</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">08/02/2017 11:10:01</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/V2Vn21hJ/51-ajustar-o-input-localizar-no-search-da-pagina" target="blank">Ajustar o input localizar no SEARCH da página.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">10/01/2017 18:16:02</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/a93lqV4a/30-retirar-a-funcionalidade-mais-filtros-da-busca-do-site-deixando-todos-os-campos-visiveis" target="blank">Retirar a funcionalidade "mais filtros" da busca do site, deixando todos os campos visiveis</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco, <br>Rafael Cruz</td>
                            <td class="lastActivity">15/12/2016 20:03:20</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/KBVmcA1I/50-removendo-o-pop-up-de-boas-festas-da-home" target="blank">Removendo o pop-up de boas festas da home.</a></td>
                            <td class="catName">Importação/Exportação/Atualização</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">04/01/2017 17:34:09</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/uHhxh5zy/3-incluir-no-section-search-o-click-fora-das-box-para-fechar" target="blank">Incluir no SECTION SEARCH (O click fora das box para fechar!)</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco, <br>Rafael Cruz</td>
                            <td class="lastActivity">15/12/2016 20:03:18</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/17faBRTU/48-criar-pop-up-na-home" target="blank">Criar pop-up na home.</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/01/2017 19:43:55</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/9Ey0Xg0U/35-corrigir-as-caracteristicas-da-pagina-single-imovel-que-estao-juntas" target="blank">Corrigir as caracteristicas da página single-imovel que estão "juntas"</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">15/12/2016 18:28:46</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/9AuFSkbH/49-implementar-a-nova-interface-no-admin" target="blank">Implementar a nova interface no admin</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">03/01/2017 19:43:47</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/iEgAW19s/34-corrigir-o-problema-das-fotos-que-estao-na-posicao-vertical-estarem-sendo-cortadas-pelo-overflow-da-div" target="blank">Corrigir o problema das fotos que estão na posição vertical estarem sendo cortadas pelo overflow da div</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">15/12/2016 18:28:41</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/nIaIjPu2/45-alterar-o-layout-do-relatorio-mensal-que-e-enviado-aos-proprietarios" target="blank">Alterar o layout do relatório mensal que é enviado aos proprietários</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">03/01/2017 12:31:41</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/V4DrK7fo/22-adicionar-na-pagina-imovel-area-para-voltar-para-a-pagina-anterior-busca" target="blank">Adicionar na página imóvel, área para voltar para a página anterior(busca).</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">14/12/2016 20:32:18</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3LFR9Itv/46-alterar-a-reciclagem-para-armazenar-de-quem-era-a-fac-que-foi-reciclada" target="blank">Alterar a reciclagem para armazenar de quem era a fac que foi reciclada</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">21/12/2016 20:02:30</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/DUqATGXR/11-acerto-na-pagina-imovel" target="blank">Acerto na página Imóvel.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">14/12/2016 20:32:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/DRhb3MzC/42-ajustar-o-layout-da-pagina-financiamento" target="blank">Ajustar o layout da página financiamento.</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">21/12/2016 19:23:00</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/oAVrsZbY/15-alterar-a-busca-simples-do-site-para-buscar-tambem-pelo-ref-do-imovel" target="blank">Alterar a busca simples do site para buscar também pelo ref do imóvel</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">06/12/2016 17:00:41</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/bNfW2hPd/2-ajustar-section-lancamentos-box-no-responsivo-quantidade-de-box-no-responsivo-incluir-vagas-garagem-area-de-valor-com-valor-loca" target="blank">Ajustar Section Lançamentos, box no responsivo, quantidade de box no responsivo, incluir vagas(garagem), área de valor com valor locação.</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco, <br>Rafael Cruz</td>
                            <td class="lastActivity">23/12/2016 11:46:25</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/roubKDBa/21-corrigir-importacao-olx-deletar-todos-e-reimportar" target="blank">Corrigir importação OLX (Deletar todos e reimportar)</a></td>
                            <td class="catName">Correção de Bug's, <br>Importação/Exportação/Atualização</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">13/12/2016 12:54:26</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/C4VmsstQ/14-alterar-as-boxes-de-imoveis-para-mostrar-o-nome-da-cidade" target="blank">Alterar as boxes de imóveis para mostrar o nome da cidade</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">06/12/2016 17:00:39</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/WmGFG5J8/36-aumentar-o-tamanho-dos-inputs-da-busca-na-area-faixa-de-preco" target="blank">Aumentar o tamanho dos inputs da busca na área "Faixa de preço"</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">20/12/2016 20:20:34</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3ZWhpM8u/19-ajuste-no-search-section-quebra-no-input-do-busca-no-responsivo" target="blank">Ajuste no SEARCH SECTION!!!Quebra no input do busca no responsivo.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">12/12/2016 21:02:23</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/vjnyKQrq/9-remover-2-banners-na-index" target="blank">Remover 2 Banners na index</a></td>
                            <td class="catName">Importação/Exportação/Atualização</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">06/12/2016 14:37:45</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/rZB1EjRo/39-acertar-o-menu-mobile-com-scroll-menu-na-proporcao-paisagem-do-celular-nao-desce" target="blank">Acertar o menu mobile com scroll, menu na proporção paisagem do celular não desce!!!</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">20/12/2016 20:20:33</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/1MaMgZPw/13-acertos-no-footer" target="blank">Acertos no FOOTER</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">12/12/2016 13:35:22</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/KEiKX91S/20-acerto-no-botao-do-menu-da-header-up" target="blank">Acerto no botão do menu Da HEADER UP</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">12/12/2016 21:02:20</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/nq6C01Om/6-ajustes-investlar" target="blank">Ajustes Investlar</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">02/12/2016 16:36:56</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/oXhPh6KF/41-1-normatizar-corretor-id-da-tabela-de-facs-1" target="blank">(1) Normatizar corretor_id da tabela de facs [1]</a></td>
                            <td class="catName">Melhoria, <br>Importação/Exportação/Atualização, <br>Desafio</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">20/12/2016 20:20:30</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/tgotFXMj/10-ajustar-imagem-do-google-maps-off" target="blank">Ajustar imagem do google maps OFF</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">06/12/2016 16:05:10</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/FWSIhnIl/8-ajustar-a-pag-imovel-no-mobile-na-div-descricao-e-entre-em-contato" target="blank">Ajustar a pág. Imóvel no mobile. Na div Descrição e Entre em contato</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">06/12/2016 16:05:12</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/IiA9NNyR/5-solicitar-a-paty-o-meu-segundo-monitor-xd" target="blank">SOLICITAR A PATY O MEU SEGUNDO MONITOR!!! xD</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco, <br>Newton Gonzaga Costa</td>
                            <td class="lastActivity">05/12/2016 13:48:36</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/JwF169Kt/12-acertar-erros-no-responsivo-da-section-lancamentos" target="blank">Acertar erros no responsivo da SECTION LANÇAMENTOS</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">12/12/2016 21:02:15</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/2v4zbvPd/33-aumentar-o-tamanho-da-fonte-do-menu-superior-do-site-agrupando-os-links-venda-locacacao-lancamento-e-exclusividade-sob-o-link-im" target="blank">Aumentar o tamanho da fonte do menu superior do Site agrupando os links "venda", "locacação", "lancamento" e "exclusividade"sob o link "Imóveis"</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">20/12/2016 20:20:32</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/VTTiZGFM/4-0-5-atualizar-os-valores-de-latitude-e-longitude-de-todos-os-imoveis" target="blank">(0.5) Atualizar os valores de latitude e longitude de todos os imóveis</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">01/12/2016 18:23:31</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/kabwH07B/18-2-implementar-integracao-olx" target="blank">(2) Implementar Integração OLX</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">12/12/2016 17:15:48</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/qIrDHBkN/investlar-scrum" target="blank">Investlar - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/gLoYT2El/17-0-5-corrigir-a-busca-mais-filtros-por-ref-do-front" target="blank">(0.5) Corrigir a busca "mais-filtros" por ref do front</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">12/12/2016 14:52:46</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/KDjoQduG/maple-bear" target="blank">Maple Bear</a></td>
                            <td class="taskName"><a href="https://trello.com/c/9pMRaXc1/1-corrigir-erros-do-google-maps-e-gerar-api-de-javascript" target="blank">Corrigir erros do google maps e gerar api de javascript</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:46:42</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/KDjoQduG/maple-bear" target="blank">Maple Bear</a></td>
                            <td class="taskName"><a href="https://trello.com/c/63f5XnjI/4-exibir-horario-de-brasilia-e-de-toronto-exibindo-o-status-de-expediente" target="blank">Exibir horário de Brasília e de Toronto . Exibindo o status de expediente.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 14:34:22</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/KDjoQduG/maple-bear" target="blank">Maple Bear</a></td>
                            <td class="taskName"><a href="https://trello.com/c/QOq4v0BA/6-menino-flutuando-em-metodologias-corrigir" target="blank">menino flutuando em metodologias, corrigir</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 19:35:25</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/KDjoQduG/maple-bear" target="blank">Maple Bear</a></td>
                            <td class="taskName"><a href="https://trello.com/c/ZjjZcQ7U/8-inserir-linha-amarela-no-modo-sticky" target="blank">inserir linha amarela no modo sticky</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 19:46:10</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/KDjoQduG/maple-bear" target="blank">Maple Bear</a></td>
                            <td class="taskName"><a href="https://trello.com/c/JxZ7DdyA/7-acima-do-menino-flutuando-existe-uma-linha-preta-sobrando-no-layout" target="blank">acima do menino flutuando, existe uma linha preta sobrando no layout</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 19:35:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/KDjoQduG/maple-bear" target="blank">Maple Bear</a></td>
                            <td class="taskName"><a href="https://trello.com/c/yzgBPFHj/2-inserir-marcador-da-maple-bear-no-mapa" target="blank">Inserir marcador da maple bear no mapa.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">16/12/2016 14:47:15</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/KDjoQduG/maple-bear" target="blank">Maple Bear</a></td>
                            <td class="taskName"><a href="https://trello.com/c/6DzC1WwU/3-no-topo-do-menu-alinhar-o-icone-ao-texto" target="blank">No topo do menu, alinhar o ícone ao texto.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 14:38:58</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/PyE4ZZA7/81-ajustar-escala" target="blank">Ajustar Escala</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">16/12/2016 21:01:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/HahNBTCD/77-colocar-a-coluna-do-telefone-do-cliente-na-listagem-de-facs" target="blank">Colocar a coluna do telefone do cliente na listagem de facs</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">16/12/2016 21:01:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/OtWVelkW/79-adicionara-coluna-de-email-do-cliente-na-lisgaem-de-facs" target="blank">Adicionara coluna de email do cliente na lisgaem de Facs</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">16/12/2016 21:01:32</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/963madrO/80-recolocar-o-campo-para-busca-de-destaque-condominio" target="blank">Recolocar o campo para busca de destaque-condominio</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">16/12/2016 21:01:29</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3M4vbCRq/69-ajustar-as-imagens-da-section-destaque" target="blank">Ajustar as imagens da section destaque!!!</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/12/2016 21:01:38</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/UNI7EJL1/78-configurar-o-site-morarbem-local" target="blank">Configurar o site Morarbem Local!</a></td>
                            <td class="catName">Importação/Exportação/Atualização</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/12/2016 16:20:31</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/GhGVEAsX/72-corrigir-o-problema-da-busca-interna-que-nao-esta-retornando-os-dados-de-acordo-com-o-parametro-integracao-destaque-old" target="blank">Corrigir o problema da Busca interna que não esta retornando os dados de acordo com o parametro integracao_destaque(old)</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">16/12/2016 12:56:13</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/8kZiCJr4/70-no-site-na-pagina-do-imovel-na-ficha-tecnica-exibir-o-nome-do-condominio-quando-tiver" target="blank">No site, na página do imóvel, na ficha técnica, exibir o nome do condomínio, quando tiver.</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">26/12/2016 14:04:12</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Ex9R2H5o/58-1-no-formulario-de-fac-ao-digitar-codigo-de-referencia-carregar-o-endereco-do-laudo-em-outro-campo-nao-editavel-1" target="blank">(1) No formulário de Fac, ao digitar código de referência, carregar o endereço do laudo em outro campo(Não editável) [1]</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">15/12/2016 20:43:50</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/YtnnW0nl/84-cadastro-de-imoveis-que-nao-vao-para-o-site" target="blank">Cadastro de imóveis que não vão para o site</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">26/12/2016 14:04:13</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/qEj1KSdK/76-na-busca-habilitar-por-busca-destaque-condominio" target="blank">Na busca, habilitar por busca destaque condomínio</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">26/12/2016 14:04:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/qa9AkID8/92-0-5-alterar-o-xml-do-zap-para-que-os-laudos-que-sao-coberturas-estejam-com-o-tipo-subtipo-cateria-apartamento-padrao-cobertura-p" target="blank">(0.5) Alterar o xml do Zap para que os laudos que são coberturas estejam com o tipo/subtipo/cateria "Apartamento Padrão/Cobertura/Padrão" [0.5]</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência, <br>Site</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">23/12/2016 13:03:00</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/xR20Oinu/22-3-implementar-o-modulo-de-escala" target="blank">(3) Implementar o Módulo de Escala</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">15/12/2016 17:56:04</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/zrOoF2fz/90-alterar-todos-oa-laudos-que-estavam-associados-a-zap-simples-para-destaques-zap" target="blank">Alterar todos oa laudos que estavam associados a zap simples para Destaques Zap</a></td>
                            <td class="catName">Melhoria, <br>Importação/Exportação/Atualização</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">22/12/2016 17:45:43</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/XrtZSEYt/30-1-ordenar-as-thumb-s-dos-single-s-de-laudos-para-bater-com-as-do-admin-2" target="blank">(1) Ordenar as thumb's dos single's de laudos para bater com as do admin [2]</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco, <br>Rafael Cruz</td>
                            <td class="lastActivity">14/12/2016 19:30:22</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/fOYjIfeA/68-copiar-funcionalidade-de-imprimir-laudo-em-branco-da-investlar" target="blank">Copiar funcionalidade de imprimir laudo em branco da investlar</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName"></td>
                            <td class="lastActivity">22/12/2016 17:08:27</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/EvmTjSF8/66-0-5-atualizar-dados-para-que-somente-os-laudos-passados-por-arquivo-em-anexo-estejam-marcados-como-infoplaca-s-0-5" target="blank">(0.5) Atualizar dados para que somente os laudos passados por arquivo em anexo estejam marcados como 'infoPLaca="s"' [0.5]</a></td>
                            <td class="catName">Importação/Exportação/Atualização</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">14/12/2016 17:41:06</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/B5kaInPV/61-0-5-no-upload-de-fotos-alterar-para-que-a-marca-d-agua-tenha-um-tamanho-menor-0-5" target="blank">(0.5) No upload de fotos, alterar para que a marca d'água tenha um tamanho menor [0.5]</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">14/12/2016 16:53:54</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/FFMqkqap/129-novo-ticket-ordenar-lista-de-fac-e-notificacao-pro-corretor-em-uma-nova-fac" target="blank">Novo Ticket! Ordenar lista de fac e Notificação pro corretor em uma nova fac</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">15/02/2017 12:41:08</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/BjaIPwy4/25-1-adicionar-telefone-e-email-do-cliente-na-busca-de-fac-1" target="blank">(1) Adicionar telefone e email do cliente na busca de FAC [1]</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">02/12/2016 20:10:33</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/fD38CRNB/88-integracoes-corrigir-acentuacao-utf8decode" target="blank">Integrações corrigir acentuação utf8decode</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">22/12/2016 17:06:48</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/lb2VQt5h/65-1-durante-o-upload-de-fotos-salvar-a-imagem-original-sem-a-marca-d-agua-por-seguranca-0-5" target="blank">(1) Durante o upload de fotos, salvar a imagem original(sem a marca d'água) por segurança [0.5]</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">14/12/2016 13:20:32</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/xCMtA0eU/159-copiar-relatorio-de-desempenho-de-corretores-da-investlar-para-morarbem" target="blank">Copiar relatório de desempenho de corretores da investlar para morarbem</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">14/02/2017 13:38:11</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/qyaA62Cz/67-salvar-a-lista-de-imoveis-que-foi-carregada-para-cada-integrador" target="blank">Salvar a lista de imóveis que foi carregada para cada integrador</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">22/12/2016 17:07:20</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3q8FD8aC/86-escala-2-cadastro-de-usuario-em-escala" target="blank">Escala 2 - Cadastro de usuário em escala</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">22/12/2016 17:06:28</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/N1R4R4F3/47-0-5-atualizar-os-valores-de-sizeareautil-dos-imoveis-que-tem-esse-valor-zerado-0-5" target="blank">(0.5) Atualizar os valores de sizeAreaUtil dos imóveis que tem esse valor zerado [0.5]</a></td>
                            <td class="catName">Importação/Exportação/Atualização</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">02/12/2016 17:10:41</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/yb6B7lEm/64-0-5-corrigir-o-erro-que-ocorre-quando-se-altera-a-ordenacao-da-busca-do-site-0-5" target="blank">(0.5) Corrigir o erro que ocorre quando se altera a ordenação da busca do site [0.5]</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">13/12/2016 21:55:12</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3Q54huRE/157-implementar-as-alteracoes-para-que-fac-funcione-corretamente" target="blank">Implementar as alterações para que Fac funcione corretamente.</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">10/02/2017 11:38:56</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/fgJOZRzU/43-0-5-corrigir-os-imoveis-que-estao-sem-latitude-e-longitude-1" target="blank">(0.5) Corrigir os imóveis que estão sem Latitude e Longitude [1]</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Newton Gonzaga Costa, <br>Heric de Honkis e Branco</td>
                            <td class="lastActivity">02/12/2016 15:16:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/1D39tZE3/85-escala-1-cadastro-de-usuario-em-turno" target="blank">Escala 1 - Cadastro de usuário em turno</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">22/12/2016 17:06:30</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/QK9HMryv/51-voltar-a-usar-a-migrations-para-controle-de-banco-1" target="blank">(?) Voltar a usar a migrations para controle de banco [1]</a></td>
                            <td class="catName">Correção de Bug's, <br>Desafios</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">13/12/2016 19:41:35</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/9sgf5KeC/120-criar-campo-no-formuario-de-laudo-para-adicionar-url-de-video" target="blank">Criar campo no formuário de laudo para adicionar url de vídeo</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">09/02/2017 19:41:43</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/xIJPtSoJ/45-0-5-tentar-adicionar-um-campo-para-filtrar-os-laudos-que-nao-tem-integracao-com-o-zap-0-5" target="blank">(0.5) Tentar adicionar um campo para filtrar os laudos que não tem integração com o zap [0.5]</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">01/12/2016 16:53:41</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/hAhTOAYY/62-0-5-criar-mecanismo-para-impedir-que-se-marque-dois-tipos-de-destaque-de-mesmo-integrador-0-5" target="blank">(0.5) Criar mecanismo para impedir que se marque dois tipos de destaque de mesmo integrador [0.5]</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">13/12/2016 17:06:50</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Rl0c5UX6/131-novo-ticket-criar-um-campo-ref-no-modulo-de-mensagens-e-adicionar-o-este-campo-para-busca" target="blank">Novo Ticket! Criar um campo ref no módulo de mensagens e adicionar o este campo para busca.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">09/02/2017 18:43:38</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/TKgujk4q/133-novo-ticket-ticket-necessita-de-explicacao-confusao-com-mensagens" target="blank">Novo Ticket! Ticket necessita de explicação, confusão com mensagens.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">09/02/2017 17:39:12</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/dALpI3Vz/44-0-5-corrigir-o-autocomplete-da-busca-do-site-0-5" target="blank">(0.5) Corrigir o autocomplete da busca do site. [0.5]</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">01/12/2016 15:15:01</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/0egg4pPk/83-log-de-integracoes" target="blank">Log de Integrações</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">21/12/2016 13:50:27</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/5L4zIqV0/63-0-5-preparar-rotina-para-corrigir-as-marcacoes-duplicadas-por-integrador-0-5" target="blank">(0.5) Preparar rotina para corrigir as marcações duplicadas por integrador [0.5]</a></td>
                            <td class="catName">Correção de Bug's, <br>Importação/Exportação/Atualização</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">13/12/2016 17:06:49</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/2gxtdT5o/126-novo-ticket-favor-colocar-o-layout-da-pagina-do-novo-imovel-igual-ao-laudo-em-branco" target="blank">Novo Ticket! Favor colocar o Layout da página do NOVO IMÓVEL , igual ao LAUDO em branco.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">31/01/2017 11:10:55</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/yFdIkUqJ/107-implementar-rotina-para-salvar-em-log-toda-e-qualquer-alteracao-feita-em-laudos" target="blank">Implementar rotina para salvar em log toda e qualquer alteração feita em Laudos.</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName"></td>
                            <td class="lastActivity">30/01/2017 12:53:26</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/6cblKUI3/145-novo-ticket-19" target="blank">Novo Ticket! #19</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">24/01/2017 13:01:44</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/gulFjEy2/154-na-home-na-area-de-sugestoes-de-imoveis-prontos-colocar-para-carregar-6-imoveis-e-inserir-na-sequencia-3-sugestoes-de-imovel-com" target="blank">Na home na área de sugestões de imóveis prontos,  colocar para carregar 6 imoveis e inserir na sequencia 3 sugestões de imóvel com permuta.</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz, <br>Newton Gonzaga Costa</td>
                            <td class="lastActivity">24/01/2017 13:01:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/ehzje15m/148-na-logo-eu-amo-rio-tirar-eu-do-coracao-e-colocar-ao-lado-esquerdo-e-apagar-o-amo" target="blank">Na logo Eu amo rio, tirar eu do coração e colocar ao lado esquerdo e apagar o amo.</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">24/01/2017 11:23:25</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/5aVhdXRZ/152-colocar-de-forma-toggle-o-banner-espaco-permuta" target="blank">Colocar de forma toggle o banner espaço permuta.</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">24/01/2017 11:23:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Jx8QjO7x/155-rodape-mudar-a-ordem-para-lancamentos-prontos-e-depois-permuta" target="blank">Rodapé mudar a ordem para lançamentos, prontos e depois permuta.</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">24/01/2017 11:23:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/CMA2ZYSj/151-na-home-colocar-um-toggle-na-area-qual-imovel-voce-procurar" target="blank">Na home colocar um toggle na area Qual imóvel você procurar?</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">24/01/2017 11:23:23</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/ib76DsqG/150-alinhar-o-navigator-de-acordo-com-a-tarja-vermelha-do-header" target="blank">Alinhar o navigator de acordo com a tarja vermelha do header.</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">24/01/2017 11:23:23</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/g9EGbx1V/149-alterar-o-titulo-principal-da-pagina-venda-ou-permute-seu-imovel" target="blank">Alterar o título principal da página "Venda ou Permute seu imóvel"</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">24/01/2017 11:23:22</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/zcT3k3Zn/142-novo-ticket-16-feito" target="blank">Novo Ticket! #16 [FEITO]</a></td>
                            <td class="catName">Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">23/01/2017 12:59:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/f8HlBUy0/57-0-5-no-formulario-de-laudos-retirar-todas-as-metragens-exeto-area-total-area-util-e-area-do-terreno-0-5" target="blank">(0.5) No formulário de laudos, retirar todas as metragens exeto "área total", "área útil" e "área do terreno" [0.5]</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">12/12/2016 12:38:55</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/BJr5aJLk/143-novo-ticket-17-feito-sem-validacao-para-caracateristica-em-condominios" target="blank">Novo Ticket! #17 [FEITO] Sem validacao para caracateristica em condominios</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 12:59:22</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/TIiqIi5s/144-novo-ticket-18-feito" target="blank">Novo Ticket! #18 [FEITO]</a></td>
                            <td class="catName">Importação/Exportação/Atualização, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">23/01/2017 12:59:17</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/PUSZOisQ/128-novo-ticket-6-feito-log" target="blank">Novo Ticket! #6 [FEITO] Log</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 12:58:33</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/5RAS93g1/130-novo-ticket-8-feito-corretor-esta-reativando-imoveis" target="blank">Novo Ticket! #8 [FEITO] Corretor esta reativando imoveis</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 12:58:49</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/JasIFad2/134-novo-ticket-12-feito-corretores-estao-acessando-blog-slides" target="blank">Novo Ticket! #12 [FEITO] Corretores estão acessando Blog/Slides</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 12:58:53</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Oe3XAaaX/132-novo-ticket-10-feito-mudar-cor-de-fonte-de-relatorio-de-desempenho-de-corretor" target="blank">Novo Ticket! #10 [FEITO] Mudar cor de fonte de relatório de desempenho de corretor</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 12:58:39</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/zNGveMLE/122-log" target="blank">Log</a></td>
                            <td class="catName">Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">23/01/2017 12:58:05</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/nGOWYPlP/147-no-header-nao-deixar-a-frase-cidade-olimpica-quebrar-para-duas-linhas" target="blank">No header, não deixar a frase cidade olímpica quebrar para duas linhas.</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">19/01/2017 19:18:31</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/uOP9a0x7/146-o-chamado-18-front-alt" target="blank">O chamado #18 FRONT-ALT</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">19/01/2017 18:04:12</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/jgYxx9wg/141-mudar-a-cor-da-fonte-do-relatorio-de-desempenho-de-corretores" target="blank">Mudar a cor da fonte do relatório de desempenho de corretores</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">17/01/2017 13:24:55</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/26gl6eoW/138-falha-de-login-por-ip-o-ip-deles-deve-ter-mudado-apos-a-acao-necessita-de-reconfiguracao-no-sistema" target="blank">Falha de login por ip (O ip deles deve ter mudado) Após a ação necessita de reconfiguração no sistema.</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">17/01/2017 13:08:52</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/FB03jRCD/140-corretores-estao-tendo-acesso-a-blog-e-sites-retirar-esses-acessos" target="blank">Corretores estão tendo acesso a blog e sites! Retirar esses acessos!</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">17/01/2017 13:01:16</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/SLfLuZMT/124-no-modulo-de-mensagens-corretor-nao-pode-ter-o-status-finalizado-na-lista-dele" target="blank">No módulo de mensagens, corretor não pode ter o status finalizado na lista dele.</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">13/01/2017 20:12:43</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/yk7pnnRT/139-corretor-esta-reativando-imoveis-que-estao-na-lixeira" target="blank">Corretor esta reativando imoveis que estão na lixeira</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">17/01/2017 12:23:42</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/sIUrd6HZ/123-marcar-por-favor-500-imoveis-nos-bairros-barra-e-recreio-com-fotos-para-classificados-do-rio" target="blank">Marcar por favor, 500 imóveis nos bairros  barra e recreio com fotos para classificados do rio.</a></td>
                            <td class="catName">Admin, <br>Intervenção Direta</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">13/01/2017 12:42:44</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3a0fQ2qi/56-0-5-copiar-a-funcionalidade-de-imprimir-laudo-da-investlar-0-5" target="blank">(0.5) Copiar a funcionalidade de imprimir laudo da investlar [0.5]</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">12/12/2016 12:38:54</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/OSjSKqYx/119-ajustar-o-layout-do-laudo-impresso" target="blank">Ajustar o layout do Laudo Impresso</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">10/01/2017 21:11:05</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/ftdCMcbl/121-colocar-o-modelo-deles-para-o-laudo-em-branco" target="blank">Colocar o modelo deles para o Laudo em branco</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">10/01/2017 18:47:48</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/jtnIGmKC/118-o-gerente-selecionado-nao-estava-aparecendo-na-listagem-de-escala" target="blank">O gerente selecionado não estava aparecendo na listagem de escala!</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">09/01/2017 15:27:08</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Xjtwwggq/117-export-de-caracteristicas-existentes" target="blank">Export de caracteristicas existentes</a></td>
                            <td class="catName">Importação/Exportação/Atualização, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">06/01/2017 20:02:46</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/8eeJbI8d/116-modificar-as-caracteristicas-do-laudo" target="blank">Modificar as caracteristicas do laudo</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">06/01/2017 20:02:45</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/0Yebof0M/113-alterar-cru-de-laudos-para-permitir-que-as-caracteristicas-possam-ser-setadas-tanto-em-laudos-como-em-condominios" target="blank">Alterar 'CRU' de laudos para permitir que as características possam ser setadas tanto em laudos como em condomínios</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">06/01/2017 20:02:44</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/NlbgTOKG/115-criar-crud-de-caractetisticas" target="blank">Criar CRUD de caractetísticas</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">06/01/2017 20:02:42</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/2U4nZEDp/114-alterar-cru-de-condominios-para-permitir-que-as-caracteristicas-possam-ser-setadas-tanto-em-laudos-como-em-condominios" target="blank">Alterar 'CRU' de condomínios para permitir que as características possam ser setadas tanto em laudos como em condomínios</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">06/01/2017 20:02:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/VfougZmB/112-0-5-novo-ticket-mensagens-restringir-usuario-criador-0-5" target="blank">(0.5) Novo Ticket! Mensagens restringir usuario criador [0.5]</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">04/01/2017 13:58:13</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/mC3y3fZ2/111-1-copiar-a-integracao-da-vivareal-para-morarbem-1" target="blank">(1) Copiar a integração da VivaReal para Morarbem [1]</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">03/01/2017 19:43:51</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/318eT2q7/109-1-criar-tabela-de-log-intermediaria-para-integracoes-1" target="blank">(1) Criar tabela de log intermediária para integrações [1]</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">03/01/2017 12:38:59</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/QGqcHGw3/103-implementar-a-nova-interface-no-admin-do-sistema" target="blank">Implementar a nova interface no admin do sistema</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">30/12/2016 13:16:09</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/pDmYPfFS/108-0-5-no-campo-busca-do-imovel-ao-preencher-o-campo-endereco-o-sr-iseu-esta-solicitando-que-tenha-um-campo-para-colocarmos-o-numer" target="blank">(0.5) No campo busca do imóvel ao preencher o campo endereço, o Sr Iseu está solicitando que tenha um campo para colocarmos o número do apartamento para que possamos fazer a busca. [0.5]</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">29/12/2016 18:28:39</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/KZ8eMbQI/106-verificar-o-save-de-lat-e-lng-do-google-no-admin" target="blank">Verificar o save de lat e lng do google no admin</a></td>
                            <td class="catName">Correção de Bug's, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">29/12/2016 16:16:08</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/cI90rPEW/105-1-ocultar-botao-x-na-lista-de-operadores-0-5" target="blank">(1) Ocultar botão x na lista de operadores [0.5]</a></td>
                            <td class="catName">Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">28/12/2016 19:17:45</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/YKNuweRs/100-trocar-aquele-olho-do-hover-dos-boxs" target="blank">Trocar aquele "olho" do hover dos boxs!!!</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">28/12/2016 15:07:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/CqBO0Mxn/89-1-adicionar-campo-status-em-integracoes-destaques-0-5" target="blank">(1) Adicionar campo status em integracoes_destaques [0.5]</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">28/12/2016 12:39:57</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/aIXrBMKa/87-0-5-criar-um-campo-abertura-de-chamado-no-formulario-da-area-de-suporte-0-5" target="blank">(0.5) Criar um campo abertura de chamado no formulário da área de suporte [0.5]</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">28/12/2016 14:04:20</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/RKt5QBkb/95-laudo-em-branco" target="blank">Laudo em Branco</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">27/12/2016 20:35:27</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/avsXbAol/74-no-sistema-na-pagina-do-imovel-na-exibicao-dos-destaques-e-integracoes-temos-que-adicionar-os-outros-destaques-do-site-lancament" target="blank">No sistema, na página do imóvel, na exibição dos destaques e integrações, temos que adicionar os outros destaques do site (lançamento e condomínio)</a></td>
                            <td class="catName">Melhoria, <br>Admin</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">26/12/2016 20:58:36</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/cWh990c9/93-escala-erro-no-texto-sem-corretores-da-chegada-chegda" target="blank">ESCALA - erro no texto sem corretores da chegada(chegda)</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">26/12/2016 20:57:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/FYEa0YLw/94-escala-incluir-um-select-de-gerente-na-inclusao-da-ordem-de-chegada" target="blank">ESCALA - incluir um select de gerente na inclusão da ordem de chegada</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">26/12/2016 20:57:23</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/15EimC6V/71-no-site-na-pagina-do-imovel-os-botoes-de-compartilhamento-nas-redes-sociais-nao-funcionam" target="blank">No site, na página do imóvel, os botões de compartilhamento nas redes sociais não funcionam</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">26/12/2016 20:36:23</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/WeUEOUKJ/97-ajustar-z-index-do-banner-morarbem-ficando-em-cima-do-email" target="blank">Ajustar z-index do banner morarbem. Ficando em cima do email.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">26/12/2016 20:36:22</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/HgME16wh/98-ajustar-logo-header-em-alguma-proporcoes-fica-de-baixo-da-logo" target="blank">Ajustar logo (header) em alguma proporções fica de baixo da logo.</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">26/12/2016 20:36:20</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/jjESFnZx/55-0-5-rever-o-cadastro-de-laudos-se-placa-esta-sendo-marcado-como-true-como-default-0-5" target="blank">(0.5) Rever o cadastro de laudos se "placa" esta sendo marcado como true como default [0.5]</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">12/12/2016 12:38:52</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/iD0JACyV/101-ajuste-no-slider-da-pagina-imovel-cor-no-thumb-ativo-e-hidden-para-fotos-verticais" target="blank">Ajuste no slider da página imóvel, cor no thumb ativo e hidden para fotos  verticais!!!</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">26/12/2016 20:36:19</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/paXkylCL/153-trocar-o-botao-espaco-permuta-para-imoveis-permuta" target="blank">Trocar o botão Espaço permuta para Imóveis permuta</a></td>
                            <td class="catName">Melhoria, <br>Site</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">23/01/2017 12:59:32</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/k6d9Bb8j/46-rever-integracoes-do-morarbem" target="blank">Rever integrações do Morarbem</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa, <br>Heric de Honkis e Branco</td>
                            <td class="lastActivity">09/12/2016 13:46:15</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/8h7oneyY/52-3-expurgar-as-integracoes-nos-laudos-3" target="blank">(3) Expurgar as integrações nos laudos [3]</a></td>
                            <td class="catName">Melhoria, <br>Desafios</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">08/12/2016 21:43:25</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/1wQWsA5h/53-0-5-ajustar-as-integracoes-para-o-novo-formato-0-5" target="blank">(0.5) Ajustar as integrações para o novo formato [0.5]</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">08/12/2016 21:43:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/kqzv3UoM/59-1-busca-do-site-nao-esta-trazendo-a-imagem-dos-imoveis-1" target="blank">(1) Busca do site não está trazendo a imagem dos imóveis [1]</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">08/12/2016 19:10:59</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/XYkd8aSF/50-2-impressao-do-laudo-em-uma-pagina-1" target="blank">(2) Impressão do Laudo em uma página [1]</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">12/12/2016 19:52:02</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/ztE5FQkV/morarbem-scrum" target="blank">Morarbem - Scrum</a></td>
                            <td class="taskName"><a href="https://trello.com/c/uLFuRLoE/60-0-5-adicionar-campo-de-integracoes-na-show-laudo-0-5" target="blank">(0.5) Adicionar campo de integracoes na show laudo [0.5]</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Newton Gonzaga Costa</td>
                            <td class="lastActivity">09/12/2016 12:39:48</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Pb1CQmRi/rotah" target="blank">RotaH</a></td>
                            <td class="taskName"><a href="https://trello.com/c/sZgme797/7-tirar-loja-penha" target="blank">Tirar loja penha.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">28/12/2016 13:52:54</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Pb1CQmRi/rotah" target="blank">RotaH</a></td>
                            <td class="taskName"><a href="https://trello.com/c/R5CrSiiH/9-tirar-as-carinhas-dos-funcionarios" target="blank">Tirar as carinhas dos funcionários.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">28/12/2016 14:00:38</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/Pb1CQmRi/rotah" target="blank">RotaH</a></td>
                            <td class="taskName"><a href="https://trello.com/c/1H9ag7Df/8-tirar-a-parte-de-contatos-formulario" target="blank">Tirar a parte de contatos(formulário).</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">28/12/2016 14:00:42</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CHmqEOVs/staff-medic" target="blank">Staff Medic</a></td>
                            <td class="taskName"><a href="https://trello.com/c/eLItx713/1-staff-jpg" target="blank">staff.jpg</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Carlos Ferreira</td>
                            <td class="lastActivity">27/12/2016 14:30:52</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/8q2Wu75r/template" target="blank">Template</a></td>
                            <td class="taskName"><a href="https://trello.com/c/mFzq5jTk/4-criar-novo-header-responsivo" target="blank">Criar novo header responsivo!</a></td>
                            <td class="catName">Emergência, <br>Melhorias FRONT-END</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">11/01/2017 11:27:07</td>
                            <td class="status">In Progress</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/8q2Wu75r/template" target="blank">Template</a></td>
                            <td class="taskName"><a href="https://trello.com/c/ADqBwVaz/1-preparar-repositorio-para-o-template-globosys-clona-modelo-do-investlar" target="blank">Preparar repositório para o template globosys(clona modelo do investlar)</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/01/2017 11:49:20</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/8q2Wu75r/template" target="blank">Template</a></td>
                            <td class="taskName"><a href="https://trello.com/c/bQTLiQNs/2-criar-novo-layout-na-base-do-investlar" target="blank">Criar novo layout na base do investlar.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">04/01/2017 12:25:42</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/8q2Wu75r/template" target="blank">Template</a></td>
                            <td class="taskName"><a href="https://trello.com/c/uQOWBcqU/3-organizando-novo-css-padrao-dos-templates-em-sass" target="blank">Organizando novo css padrão  dos templates em SASS!!!</a></td>
                            <td class="catName">Melhorias FRONT-END</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">11/01/2017 11:27:04</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/4bHi6PfV/zello" target="blank">Zello</a></td>
                            <td class="taskName"><a href="https://trello.com/c/FCPOGvqA/1-desenvolver-plugin-para-consulta-a-tabela-fipe" target="blank">Desenvolver plugin para consulta a tabela fipe</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">27/12/2016 14:44:10</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/KZuBaTDw/3-configurar-controladores-para-o-arquivo-home-php" target="blank">Configurar controladores para o arquivo home.php</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">19/01/2017 17:40:36</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/WRejZAMj/14-inserir-icones-de-linguagem-portugues-br-e-ingles-us-de-acordo-com-o-layout-desenvolvido-pela-criacao" target="blank">Inserir ícones de linguagem Português-BR e Inglês-US de acordo com o layout desenvolvido pela criação.</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 20:48:50</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/ZZmxLCsN/8-corrigir-a-formatacao-do-bloco-em-destaque-no-header" target="blank">Corrigir a formatação do bloco em destaque no header</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 14:35:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3QhPDG9J/12-padronizar-uma-main-class-no-css-para-os-botoes-da-home-e-adequar-os-elementos-before-ao-modo-responsivo" target="blank">Padronizar uma Main Class no css para os botões da home. E adequar os elementos :before ao modo responsivo</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">30/01/2017 19:33:26</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/TVdwGlZN/13-menu-principal-topo-organizar-as-classes-que-controlam-o-flex-padronizar-uma-classe-para-o-menu-da-home-e-adequar-ao-modo-respon" target="blank">Menu principal (topo). Organizar as classes que controlam o flex. Padronizar uma classe para o menu da home e adequar ao modo responsivo. Temos 4 classes trabalhando nos menus da home atualmente. Executando um mesmo serviço</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">30/01/2017 19:33:31</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/L0nuabHw/15-sobre-o-produto-em-destaque-na-homepage-vamos-alterar-ou-remover-o-bloco-do-produto-seguindo-o-feedback-da-criacao-que-informou-" target="blank">Sobre o produto em destaque na homepage? Vamos alterar ou remover o bloco do produto? Seguindo o feedback da criação que informou a desistência por parte do cliente de trabalhar com o material que usamos no exemplo.</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 20:49:03</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/C4eBihH9/7-corrigir-a-estilizacao-do-menu" target="blank">Corrigir a estilização do menu</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 14:35:06</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/I0E6oxog/5-configurar-o-servidor-node-o-compass-sass-e-o-grunt-js-o-dev-desenvolveu-com-css-compilado" target="blank">Configurar o servidor node, o compass (sass), e o grunt.js. O dev desenvolveu com css compilado.</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">19/01/2017 18:07:02</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/p1igxePD/54-adiconar-um-campo-personalizado-ao-post-type-webdoor" target="blank">Adiconar um campo personalizado ao post type webdoor</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">23/01/2017 20:49:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/0D2w3dbg/4-inserir-efeito-gradiente-por-cima-do-banner-na-homepage" target="blank">Inserir efeito gradiente por cima do banner na homepage</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">19/01/2017 18:07:09</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/CL5ytMKx/infochd" target="blank">infoCHD</a></td>
                            <td class="taskName"><a href="https://trello.com/c/j9RGjChG/52-criar-campo-personalizado-para-o-post-type-webdoor-banner-da-home" target="blank">Criar campo personalizado para o post type Webdoor (banner da home)</a></td>
                            <td class="catName"></td>
                            <td class="memberName"></td>
                            <td class="lastActivity">25/01/2017 18:28:31</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/P423R30q/39-reformular-o-header-parase-integrar-melhor-ao-site" target="blank">Reformular o header "parase integrar melhor ao site"</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/02/2017 15:45:18</td>
                            <td class="status">In Progress</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/l6WgXF8a/38-reformular-o-formulario-de-contato-na-tela-de-imovel" target="blank">Reformular o formulário de contato na tela de imóvel</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/02/2017 15:44:57</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/nXpQlSMJ/42-ha-alguma-maneira-de-comprimir-a-busca-no-mobile-para-que-apareca-mais-dela-numa-tela-so" target="blank">Há alguma maneira de comprimir a busca no mobile para que apareça mais dela numa tela só?</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/02/2017 15:45:56</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/IjQ2Z88I/33-fazer-a-busca-ocupar-somente-uma-linha-sumindo-com-as-opcoes-quartos-garagens-e-suites-e-adicionando-um-botao-de-mais-informacoe" target="blank">Fazer a busca ocupar somente uma linha(sumindo com as opçoes quartos, garagens e suites e adicionando um botão de mais informações(parecido com a do zap))</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/02/2017 15:42:41</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/odFReACH/36-diminuir-a-foto-e-o-nome-do-corretor-na-pagina-de-imovel" target="blank">Diminuir a "foto" e o nome do corretor na página de imóvel</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/02/2017 15:42:07</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Xq5QP4A4/35-retirar-a-descida-de-pagina-que-ocorre-na-isualizacao-do-imovel" target="blank">Retirar a "descida de página" que ocorre na isualização do imóvel</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/02/2017 15:42:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/JNe8ExJ7/40-no-mobile-quando-a-referencia-e-colocada-a-parte-que-escreve-desaparece-com-o-scroll-do-teclado-somente-no-mobile-um-botao-de-bu" target="blank">No Mobile, quando a referência é colocada a parte que escreve desaparece com o scroll do teclado. Somente no mobile, um botão de busca deve ser colocado ao lado da referência pra facilitar o uso.</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">03/02/2017 15:39:52</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/vOjFSeI8/22-quem-determina-os-destaques-que-sao-colocados-ali-na-pagina-inicial-nao-sao-imoveis-muito-atrativos-visto-que-as-pessoas-pouco-c" target="blank">Quem determina os destaques que são colocados ali na página inicial? Não são imóveis muito atrativos, visto que as pessoas pouco clicam neles.</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">03/02/2017 15:40:12</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/yZDJO1Tp/34-verificar-e-corrigir-erros-do-modal-inicial-em-algumas-resolucoes-de-celular" target="blank">Verificar e corrigir erros do modal inicial em algumas resoluções de celular</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">25/01/2017 14:34:07</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/dLRF1dGR/16-na-section-historico-de-pesquisa-nao-alterna-os-imoveis-de-acordo-com-historico" target="blank">Na Section Histórico de pesquisa não alterna os imóveis de acordo com histórico.</a></td>
                            <td class="catName">Desafio</td>
                            <td class="memberName">Heric de Honkis e Branco, <br>Newton Gonzaga Costa</td>
                            <td class="lastActivity">25/01/2017 14:00:00</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/8sw2wfjg/23-como-o-historico-e-determinado-e-o-historico-de-todos-os-usuarios-juntos-nao-vejo-ele-mudando-conforme-meu-uso" target="blank">Como o histórico é determinado? É o histórico de todos os usuários juntos? Não vejo ele mudando conforme meu uso.</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Heric de Honkis e Branco</td>
                            <td class="lastActivity">25/01/2017 13:59:39</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/OKddCah7/21-slider-fullscreen-nao-pode-ficar-ali-isso-faz-com-que-hajam-90-de-dropouts-visto-que-o-usuario-nao-sabe-o-que-fazer-depois-de-ve" target="blank">Slider fullscreen NÃO PODE FICAR ALI. Isso faz com que hajam 90% de dropouts, visto que o usuário não sabe o que fazer depois de ver um slider fullscreen, pouquíssimos fizeram o scroll. Vamos tirar a busca de baixo e colocar ela em cima, NO MEIO DA PÁGINA, onde supostamente o blog deveria ficar. Só assim informaremos corretamente o usuário em relação a localização da busca.</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">17/01/2017 19:09:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/UPdDza6q/27-no-celular-diminuir-o-espaco-exagerado-entre-as-caracteristicas-na-ficha-tecnica-isso-tudo-pode-ser-um-pouco-menor-e-assim-compa" target="blank">No celular, diminuir o espaço exagerado entre as características na ficha técnica, isso tudo pode ser um pouco menor e assim compactarmos as informações, assim como tirar o botão de ver a localização do imóvel, e tirar este ícone ENORME dos corretores que não diminui responsivamente.</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">17/01/2017 17:05:16</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/BlPQcURe/32-retirar-margin-top-do-corretorbox-na-pagina-de-imovel-na-proporcao-mobile" target="blank">Retirar margin-top do corretorBox na página de imóvel na proporção mobile!</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">17/01/2017 14:40:42</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/pyO4YKV8/26-na-pagina-de-imoveis-comprimimos-o-header-no-estilo-do-sticky-que-ja-existe-mas-claro-centralizado-sem-essas-sombras-exageradas-" target="blank">Na página de imóveis, comprimimos o header no estilo do sticky que já existe, mas, claro, centralizado, sem essas sombras exageradas e com fontes mais bem pensadas. Sem seguir o usuário, fica lá em cima bem compacto. Tiramos também a foto que fica entre o header e o imóvel, deixando o usuário vendo diretamente o imóvel que queria sem precisar fazer scroll. O scroll está nos causando 10% de drop-out nos primeiros 10 segundos.</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">17/01/2017 14:22:23</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/UvYI6O2o/31-ajustar-o-header-para-centralizar-os-telefones" target="blank">AJustar o header para centralizar os telefones.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/01/2017 19:27:58</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/nVPxVkLq/24-o-box-de-seja-bem-vindo-tem-fontes-erradas-no-texto-superior-e-a-parte-de-vender-nao-esta-responsiva-tivemos-muitos-cliques-em-v" target="blank">O box de "Seja bem-vindo" tem fontes erradas no texto superior e a parte de "vender" não está responsiva, tivemos muitos cliques em vender e perdemos muito com isso, o cara simplesmente SAI DA PÁGINA.</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/01/2017 20:40:16</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/Vlyg1q9G/14-ajuste-no-formulario-da-pagina-imovel-tirando-a-largura-100-e-deixando-bicolunado" target="blank">Ajuste no formulário da página imóvel, tirando a largura 100% e deixando bicolunado!!!</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/01/2017 20:40:58</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/s8VrcxgE/18-erro-nas-box-de-imoveis-na-numeracao-de-vagas-banheiros-e-quartos" target="blank">Erro nas box de imoveis. Na numeração de vagas, banheiros e quartos.</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz, <br>Heric de Honkis e Branco</td>
                            <td class="lastActivity">16/01/2017 18:36:14</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/R5DwJLtI/25-o-sticky-header-foi-uma-ideia-ruim-minha-temos-que-tira-lo-pois-o-cara-que-tem-um-monitor-de-baixa-resolucao-se-sente-irritado-c" target="blank">O sticky header foi uma idéia ruim minha. Temos que tirá-lo pois o cara que tem um monitor de baixa resolução se sente irritado com o tamanho daquilo seguindo ele.</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/01/2017 18:26:18</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/wbaLiGYX/20-corretor-nao-e-consultor-nao-ha-consultores-na-styllus-imobiliaria" target="blank">Corretor não é consultor. Não há consultores na Styllus Imobiliária.</a></td>
                            <td class="catName">Correção de Bug's, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/01/2017 17:15:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/0v0npNW4/29-acertar-as-cores-dos-icones-do-header-facebook-e-instagram" target="blank">Acertar  as cores dos  ícones do header (FACEBOOK E INSTAGRAM</a></td>
                            <td class="catName">Correção de Bug's, <br>Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/01/2017 17:52:04</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/KwRl7DKV/1-aplicando-efeito-na-logo-e-ajustando-responsivo" target="blank">Aplicando efeito na logo e ajustando responsivo.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">05/01/2017 16:13:29</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/nL1431gC/19-criar-modal-bem-vindo" target="blank">Criar Modal bem-vindo!</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/01/2017 16:13:58</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/jvbcAtIF/8-na-pagina-de-contatos-no-formulario-retirar-loja-meier-do-select-ja-selecionado-para-selecione-uma-loja" target="blank">Na página de contatos, no formulário, retirar Loja Méier do select já selecionado para "Selecione uma loja"</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">06/01/2017 20:07:25</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/1wx4qHGo/15-na-pagina-trabalhe-conosco-nao-funciona-o-banner-principal" target="blank">Na página trabalhe conosco, não funciona o banner principal.</a></td>
                            <td class="catName">Correção de Bug's</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">13/01/2017 11:40:30</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/9YX5o7Pl/17-trocar-a-logo-horizontal-styllus-no-header" target="blank">Trocar a logo horizontal styllus no header!!!</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">13/01/2017 11:39:31</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/sJxttsXx/30-ajustar-favoritar-muito-grande-no-mobile" target="blank">Ajustar Favoritar, muito grande no mobile!!!</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">16/01/2017 20:37:21</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/YkKarexS/13-ajustar-a-section-de-financiamento-da-pagina-de-financiamento-troca-dos-icones-dos-bancos-e-lincar-para-os-financiamentos-do-ban" target="blank">Ajustar a section de financiamento da página de financiamento!!!troca dos ícones dos bancos e lincar para os financiamentos do banco de origem.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">06/01/2017 20:08:01</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3npPrymJ/5-melhoria-nos-boxs-dos-imoveis" target="blank">Melhoria nos boxs dos imóveis.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">06/01/2017 17:30:52</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/yt1zElEb/11-diminuir-a-altura-do-header-numa-boa-proporcao-para-mobile-trocar-o-botao-do-menu-ajuste-na-proporcao-tablet" target="blank">Diminuir a altura do header, numa boa proporção para mobile. Trocar o botão do menu. ajuste na proporção tablet.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">06/01/2017 20:07:20</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/L0AEJKFx/2-ajuste-do-header" target="blank">Ajuste do Header.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">05/01/2017 16:14:18</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/EoTGCPAZ/12-ajustar-urgente-o-menu-mobile-acerto-de-cores-e-de-hover-e-action" target="blank">ajustar urgente, o menu mobile. acerto de cores e de hover e action.</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">06/01/2017 17:31:10</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/YnpAa16e/3-acerto-nas-cores-e-dos-sliders-do-search-com-responsivo" target="blank">Acerto nas cores e dos sliders do SEARCH com responsivo.</a></td>
                            <td class="catName"></td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">05/01/2017 16:30:24</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/0LAvZDrp/9-retirar-a-cor-amarelo-do-valor-do-imovel-na-section-historico-de-pesquisa" target="blank">Retirar a cor amarelo do valor do imóvel na section histórico de pesquisa.</a></td>
                            <td class="catName">Melhoria, <br>Emergência</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">05/01/2017 20:01:08</td>
                            <td class="status">Done</td>
                        </tr>
                    
                        <tr>
                            <td class="boardName"><a href="https://trello.com/b/zbgRDdE8/styllus" target="blank">styllus</a></td>
                            <td class="taskName"><a href="https://trello.com/c/3lsI1mfY/10-ajuste-na-diagramacao-na-etapa-de-enderecos-das-lojas-no-footer" target="blank">Ajuste na diagramação na etapa de endereços das lojas no footer.</a></td>
                            <td class="catName">Melhoria</td>
                            <td class="memberName">Rafael Cruz</td>
                            <td class="lastActivity">05/01/2017 19:27:46</td>
                            <td class="status">Done</td>
                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</section></div>
@endsection
