@extends('layouts.app')

@section('content')
<section class="box ">
    <header class="panel_header">
        <h2 class="title pull-left">{{$subarea_name}}</h2>
        <div class="actions panel_actions pull-right">
        <a class="btn btn-default btn-icon" href="{{ url('/'.App::getLocale().'/'.explode('/', Route::current()->uri)[1].'/create') }}"  style="padding:7px 18px; color:#333333; font-size: 14px;"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{__('app.add')}}</a>
        <?php /*<div class="actions panel_actions pull-right">
            <a class="box_toggle fa fa-chevron-down"></a>
            <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
            <a class="box_close fa fa-times"></a>*/ ?>
            
        </div>
    </header>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                @include(explode('/', Route::current()->uri)[1].'._list')            
            </div>
        </div>
    </div>
</section></div>
@endsection
