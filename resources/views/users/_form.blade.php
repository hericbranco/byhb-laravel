<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			{!! Form::label('name', 'Nome', ['class' => 'form-label']) !!}			
			{!! Form::text('name', null, ['class' => 'form-control']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('email', 'Email', ['class' => 'form-label']) !!}			
			{!! Form::text('email', null, ['class' => 'form-control']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('profile_id', 'Perfil', ['class' => 'form-label']) !!}			
			{!! Form::select('profile_id', $arrPerfil, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('password', 'Senha', ['class' => 'form-label']) !!}			
			{!! Form::password('password', ['class' => 'form-control']) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('password_confirmation', 'Confirme a senha', ['class' => 'form-label']) !!}			
			{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}			
		</div>
	</div>
</div>