<table id="searchTable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>Nome</td>
            <td>Slug</td>
            <td>Status</td>
            <td>Criado</td>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($arrObjData AS $objData)
            <tr>
                <td>{{$objData->name}}</td>
                <td>{{$objData->slug}}</td>
                <td>{{$objData->status}}</td>
                <td>{{$objData->created_at}}</td>
                <td>@include('layouts._list_actions', ['objData' =>$objData])</td>
            </tr>
        @empty

        @endforelse
    </tbody>

</table>