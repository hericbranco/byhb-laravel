<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{

	protected $fillable = ['name'];
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	public function setNameAttribute($value) {
		$this->attributes['name'] = $value;
		
		$this->attributes['slug'] = str_slug($this->name);

	}

	public function getCreatedAtAttribute()
	{
		return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d/m/Y H:i:s');
	}

	public function getStatusAttribute() 
	{
		return ($this->deleted_at == '')? 'ativo' : 'inativo' ;
	}
}
