<?php

namespace App;

use App\UsersExtraFields;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name', 'email', 'password', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    public function getStatusAttribute() 
    {
        return ($this->deleted_at == '')? 'ativo' : 'inativo' ;
    }


    public function extraFields()
    {
        return $this->belongsToMany(UsersExtraFields::class, 'users_extra_fields_values', 'users_id', 'users_extra_fields_id')->withPivot('value');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'profile_id');
    }
}
