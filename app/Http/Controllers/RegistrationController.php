<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Lang;

class RegistrationController extends Controller
{
	public function create() {
		return view('auth.register');
	}


	public function store() {
		
		$this->validate(request(), [
			'name' => 'required',
			'email' => 'required|email',
			'password' => 'required|confirmed',

		]);

		$arr  = array_merge(request(['name', 'email', 'password']), ['api_token' =>  str_random(60)]);
		//dd($arr);
		$user = User::create($arr);

		auth()->login($user);

		//return redirect('https://trello.com/1/authorize?callback_method=fragment&return_url=http://globosys-taskmanager.dev/updateTrelloToken&scope=read&expiration=never&name=Globosys TaskManager&key=9d0718270b85d945107b2cbf38464b06');
		return redirect(Lang::getLocale().'/purchase');

	}

	public function storeToken(User $user) {
		if (\Auth::user()) {
			$user->extraFields()->sync(['1', ['value' => request('token')]]);
			return redirect('home');
		}
	}


}
