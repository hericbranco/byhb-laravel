<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area_name = 'Perfil';
        $subarea_name = 'Listagem';
        $arrObjData = Profile::withTrashed()->get();

        return view('list', compact('area_name', 'subarea_name', 'arrObjData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area_name = 'Perfil';
        $subarea_name = 'Cadastro';
        return view('form', compact('area_name', 'subarea_name'));
    }

    public function save()
    {
        if (request()->id) 
        {
            $this->update();
        } else {
            $this->store();
        }
        
        return redirect('profile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'name'        => 'required',
            ]);

        Profile::create(request(['name']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        $area_name = 'Perfil';
        $subarea_name = 'Cadastro';
        $objData = $profile;
        return view('form', compact('area_name', 'subarea_name', 'objData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $this->validate(request(), [
            'name' => 'required'
            ]);

        $profile = Profile::find(request()->id);
        $profile->fill(request(['name']));
        $profile->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {        
        $profile->delete();
        return redirect('profile');
    }
}
