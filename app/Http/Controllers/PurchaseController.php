<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\Product;
use App\Client;
use Illuminate\Http\Request;
use Lang;

class PurchaseController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area_name = __('app.purchases');
        $subarea_name = __('app.list');
        $arrObjData = Purchase::get();

        if (request()->segments()[0] == 'api') {
            return response()->json($arrObjData);   
        }

        return view('list', compact('area_name', 'subarea_name', 'arrObjData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area_name = __('app.purchases');
        $subarea_name = __('app.register');

        $arrClient = Client::get()->pluck('name', 'id')->toArray();
        $arrProduct = Product::get()->pluck('name', 'id')->toArray();

        return view('form', compact('area_name', 'subarea_name', 'arrClient', 'arrProduct'));
    }

    public function save()
    {
        if (request()->id) 
        {
            $arrObjData = $this->update();
        } else {
            $arrObjData = $this->store();
        }

        if (request()->segments()[0] == 'api') {
            return response()->json($arrObjData, 201);   
        }
        
        return redirect(Lang::getLocale().'/purchase');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'product_id'        => 'required',
            'client_id' => 'required',
            'quantity' => 'required|numeric'
            ]);

        return Purchase::create(request(['client_id', 'product_id', 'quantity', 'status']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        if (request()->segments()[0] == 'api') {
            return response()->json(Purchase::with('client', 'product')->first(request()->id), 201);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        $area_name = __('app.purchases');
        $subarea_name = __('app.register');
        $objData = $purchase;

        $arrClient = Client::get()->pluck('name', 'id')->toArray();
        $arrProduct = Product::get()->pluck('name', 'id')->toArray();

        return view('form', compact('area_name', 'subarea_name', 'objData', 'arrClient', 'arrProduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $this->validate(request(), [
            'product_id'        => 'required',
            'client_id' => 'required',
            'quantity' => 'required|numeric'
            ]);

        $purchase = Purchase::find(request()->id);
        $purchase->fill(request(['quantity', 'client_id', 'product_id', 'status']));
        $purchase->save();
        return Purchase::with('client', 'product')->find(request()->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {        
        $purchase->delete();
        if (request()->segments()[0] == 'api') {
            return response()->json([], 200);
        }
        return redirect(Lang::getLocale().'/purchase');
    }
}
