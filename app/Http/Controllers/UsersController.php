<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area_name = 'Usuários';
        $subarea_name = 'Listagem';
        $arrObjData = User::get();

        if (request()->segments()[0] == 'api') {
            return response()->json($arrObjData);   
        }

        return view('list', compact('area_name', 'subarea_name', 'arrObjData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area_name = 'Perfil';
        $subarea_name = 'Cadastro';
        $arrPerfil = Profile::get()->pluck('name', 'id')->toArray();
        return view('form', compact('area_name', 'subarea_name', 'arrPerfil'));
    }

    public function save()
    {
        if (request()->id) {
            $this->update();
        } else {
            $this->store();
        }
        
        return redirect('users');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'profile_id' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            ]);
        $arr  = array_merge(request(['name', 'email', 'password']), ['api_token' =>  str_random(60)]);
        //dd($arr);
        $user = User::create($arr);
        //$user = User::create(request(['name', 'email', 'password']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $area_name = 'Perfil';
        $subarea_name = 'Cadastro';
        $objData = $user;
        $arrPerfil = Profile::get()->pluck('name', 'id')->toArray();
        return view('form', compact('area_name', 'subarea_name', 'objData', 'arrPerfil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $this->validate(request(), [
            'name' => 'required',
            'profile_id' => 'required',
            'email' => 'required|email',
            'password' => 'confirmed',
            ]);

        $arrOptions = request(['name', 'email', 'password']);
        if ($arrOptions['password'] == '') {
            unset($arrOptions['password']);
        }

        $objData = User::find(request()->id);
        $objData->fill($arrOptions);
        $objData->profile()->associate(Profile::find(request()->profile_id));        
        $objData->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('users');
    }
}
