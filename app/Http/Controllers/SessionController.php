<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
	public function destroy() {
		auth()->logout();
		return redirect('home');
	}

	public function create() {
		return view('auth.login');
	}

	public function store() {
		//dd(request());

		if (!auth()->attempt(request(['email', 'password']))) {
			return back();
		}

		return redirect('purchase');

	}
}
