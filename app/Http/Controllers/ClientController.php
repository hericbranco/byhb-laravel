<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Lang;

class ClientController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area_name = __('app.clients');
        $subarea_name = __('app.list');
        $arrObjData = Client::get();

        if (request()->segments()[0] == 'api') {
            return response()->json($arrObjData);   
        }

        return view('list', compact('area_name', 'subarea_name', 'arrObjData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area_name = __('app.clients');
        $subarea_name = __('app.register');
        return view('form', compact('area_name', 'subarea_name'));
    }

    public function save()
    {
        if (request()->id) 
        {
            $arrObjData = $this->update();
        } else {
            $arrObjData = $this->store();
        }

        if (request()->segments()[0] == 'api') {
            return response()->json($arrObjData, 201);   
        }
        
        return redirect(Lang::getLocale().'/client');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'name'        => 'required',
            'email' => 'required|email',
            'cpf' => 'required|digits:11'
            ]);

        return Client::create(request(['name', 'email', 'cpf']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        if (request()->segments()[0] == 'api') {
            return response()->json($client, 201);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $area_name = __('app.clients');
        $subarea_name = __('app.register');
        $objData = $client;
        return view('form', compact('area_name', 'subarea_name', 'objData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'cpf' => 'required|digits:11'
            ]);

        $client = Client::find(request()->id);
        $client->fill(request(['name', 'email', 'cpf']));
        $client->save();
        return Client::find(request()->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {        
        $client->delete();
        if (request()->segments()[0] == 'api') {
            return response()->json([], 200);
        }
        return redirect(Lang::getLocale().'/client');
    }
}
