<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Lang;

class ProductController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area_name = __('app.products');
        $subarea_name = __('app.list');
        $arrObjData = Product::get();

        if (request()->segments()[0] == 'api') {
            return response()->json($arrObjData);   
        }

        return view('list', compact('area_name', 'subarea_name', 'arrObjData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $area_name = __('app.products');
        $subarea_name = __('app.register');
        return view('form', compact('area_name', 'subarea_name'));
    }

    public function save()
    {
        if (request()->id) 
        {
            $arrObjData = $this->update();
        } else {
            $arrObjData = $this->store();
        }

        if (request()->segments()[0] == 'api') {
            return response()->json($arrObjData, 201);   
        }
        
        return redirect(Lang::getLocale().'/product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'name'        => 'required',
            'price' => 'required|numeric',
            'barCode' => 'required|numeric|digits_between:1,20'
            ]);

        return Product::create(request(['name', 'price', 'barCode']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        if (request()->segments()[0] == 'api') {
            return response()->json($product, 201);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $area_name = __('app.products');
        $subarea_name = __('app.register');
        $objData = $product;
        return view('form', compact('area_name', 'subarea_name', 'objData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $this->validate(request(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'barCode' => 'required|numeric|digits_between:1,20'
            ]);

        $product = Product::find(request()->id);
        $product->fill(request(['name', 'price', 'barCode']));
        $product->save();
        return Product::find(request()->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {        
        $product->delete();
        if (request()->segments()[0] == 'api') {
            return response()->json([], 200);
        }
        return redirect(Lang::getLocale().'/product');
    }
}
