<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
    'product_id', 'client_id', 'quantity', 'status'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }
}


