<?php

use Illuminate\Database\Seeder;

class UsersExtraFieldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_extra_fields')->insert([
        	'name' => 'trello_token',
        	'type' => 'hidden',
        	'is_required' => '0',
            'created_at' => date('Y-m-d H:i:s')
        	]);
    }
}
