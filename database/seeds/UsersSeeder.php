<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'profile_id' => '1',
            'name' => 'Heric',
            'email' => 'hericbranco@gmail.com',
            'password' => bcrypt('123456'),
            'api_token' => str_random(60),
            'created_at' => date('Y-m-d H:i:s')
            ]);

        DB::table('users_extra_fields_values')->insert([
        	'users_id' => 1,
        	'users_extra_fields_id' => 1,
        	'value' => '5f73d1a67bad94bbb4f1355344759d54b70dbea5179ef3c1ea05a43b02bf5f82'                           
            ]);

        DB::table('users')->insert([
            'profile_id' => '1',
            'name' => 'Newton',
            'email' => 'ncosta.corp@gmail.com',
            'api_token' => str_random(60),
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d H:i:s')
            ]);

        DB::table('users_extra_fields_values')->insert([
            'users_id' => 2,
            'users_extra_fields_id' => 1,
            'value' => '0e28f803d1c3fcd09ad991c9687a20b15ab9e8001420e4f9b6adcf2fc2e0eae9'                           
            ]);

        DB::table('users')->insert([
            'profile_id' => '1',
            'name' => 'Contato Dagema',
            'email' => 'contato@dagemapublicidade.com',
            'password' => bcrypt('123456'),
            'api_token' => str_random(60),
            'created_at' => date('Y-m-d H:i:s')
            ]);

        DB::table('users_extra_fields_values')->insert([
            'users_id' => 3,
            'users_extra_fields_id' => 1,
            'value' => '5ad5a5aee9e9edd185c52f7ec572ef65974dba321cb974a3facd501211636f81'                           
            ]);
    }
}