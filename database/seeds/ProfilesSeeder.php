<?php

use Illuminate\Database\Seeder;

class ProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    		$arrArrProfile = [
    			['name' => 'Admin', 'slug' => 'admin'], 
    			
    		];

    		foreach ($arrArrProfile AS $arrProfile) 
    		{
    			DB::table('profiles')->insert(array_merge($arrProfile, ['created_at' => date('Y-m-d H:i:s')]));
    		}
    }
}
