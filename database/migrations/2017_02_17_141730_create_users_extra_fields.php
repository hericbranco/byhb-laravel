<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersExtraFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_extra_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', ['text', 'hidden', 'select', 'textarea', 'file'])->default('text');            
            $table->boolean('is_required')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_extra_fields_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_extra_fields_id')->unsigned();
            $table->string('name');
            $table->string('value');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('users_extra_fields_id')->references('id')->on('users_extra_fields')->onDelete('cascade');
        });

        Schema::create('users_extra_fields_values', function (Blueprint $table) {
            $table->integer('users_id')->unsigned();
            $table->integer('users_extra_fields_id')->unsigned();            
            $table->text('value');
            $table->foreign('users_extra_fields_id')->references('id')->on('users_extra_fields')->onDelete('cascade');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_extra_fields_values');
        Schema::dropIfExists('users_extra_fields_options');
        Schema::dropIfExists('users_extra_fields');
    }
}
